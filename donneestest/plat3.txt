
Couscous poulet et merguez facile
2–3 minutes


merguez

huile d'olive

harissa

tomate

carotte

courgette

pois chiches

pilon de poulet

8

pilons de poulet (fermier de préférence)

concentré de tomates

2 boîtes

de concentré de tomates

épices à couscous

3 c.à.s

d'épices à couscous

navet

Cuillère en bois

1 Cuillère en bois

Temps total: 1 h 5 min

Préparation:
30 min

Repos:
-

Cuisson:
35 min

étape n°1

Lavez et épluchez les carottes, les navets et les tomates et les couper en cubes.

    Étape 1

    Lavez et épluchez les carottes, les navets et les tomates et les couper en cubes.
    Étape 2

    Dans un autocuiseur, versez 3 cuillères à soupe d'huile d'olive. Mettez à chauffer et faites-y dorer les pilons de poulet. Quand ils sont dorés, versez l'équivalent d'1 litre d'eau et ajouter les cubes de bouillon de boeuf, le concentré de tomate, les carottes, navets et tomates, les épices à couscous et l'harissa.
    Étape 3

    Fermez votre autocuiseur et compter 25 mn de cuisson une fois que celui-ci est monté en pression.
    Étape 4

    En attendant, lavez et coupez les courgettes en cubes et égouttez les pois chiches.
    Étape 5

    courgette

    pois chiches

    Une fois les 25 mn écoulées, évacuez la vapeur de votre autocuiseur, ouvrez et rajouter les courgettes et les pois chiches.
    Étape 6

    Remettez au feu : comptez 10 mn de cuisson une fois votre autocuiseur sous pression.
    Étape 7

    merguez

    Faites cuire vos merguez sur un grill ou à la poêle, mais pas avec les légumes.
    Étape 8

    Servez accompagné d'une semoule fine.

