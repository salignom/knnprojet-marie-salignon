
Petits pâtés chauds en croûte (pâté en croûte) - Recette par Chef Simon
3–4 minutes
Ingrédients

Pour 2 personnes
Recette de pâtés en croûte pour 2 personnes

100 g de hachis porc et veau

50 g de gésiers de canard confits

salade assaisonnée légèrement aillée (huile d'olive, Xérès et balsamique et éclat d'ail).

Convertir les mesures ou températures
Préparation de la recette

3

Sur deux disques, poser au centre l’équivalent d’une balle de ping pong de farce.

4

Badigeonner les bords avec un peu d’eau.

5

Poser le deuxième disque sur la farce et presser les bords.

6

Faites un trou au sommet et souder à nouveau les bords.

7

Badigeonner avec un jaune d’œuf légèrement détendu à l’eau.

8

Laisser au froid pendant une à deux heures.

9

Cuire les oreillers au four réglé à 180°C pendant 15 à 20 minutes.

10

Servir avec la salade assaisonnée.




