# KNNprojet - Marie SALIGNON

## A propos

Ce projet permet de développer des classes (ici deux) afin de mettre en oeuvre une classification KNN en langage Python.

## Prérequis 

Pour le bon fonctionnement du code certaines fonctions de base sont nécessaires pour son utilisation.
On importe certains modules :

import json = permet de convertir un dictionnaire Python en une chaine JSON qui peut être écrite dans un fichier.
import math = permet d'étendre la liste des fonctions mathématiques
import re = permet d'utiliser les fonctions régulières en Python

Certaines fonctions ont aussi été ajoutées au code pour permettre son bon fonctionnement : 

def vectorise(tokens):
	"""
    But : cette fonction récupère une liste de tokens, et renvoie un dictionnaire
    contenant le vocabulaire avec les fréquences associées
    Input :
    arg1 : tokens - list(str)
    Output :
    valeur de retour : un dict contenant les vocables (clés) et les fréq associées (valeur)
	"""

tok_grm=re.compile(r"""
    (?:etc.|p.ex.|cf.|M.)|
    \w+(?=(?:-(?:je|tu|ils?|elles?|nous|vous|leur|lui|les?|ce|t-|même|ci|là)))|
    [\w\-]+'?| 
    """,re.X)
	
def tokenize(text,tok_grm):
  """
  Input : 
    arg1 : un texte à tokeniser
    arg2 : une regex compilée pour la tokenisation
  Output : 
    valeur de retour : la liste des tokens
  """

## Utilisation

### Création d'une base documentaire

Pour mettre en place les classes, on créer des groupes (exemple de test ici = "classes") de textes qui sont similaires. Ici, on a trois groupes différents de textes qui prennent en compte des thèmes semblables. 

### Ajout des fichiers dans les classes

Pour ajouter chaque texte de chaque groupe dans la KNNClasses, on fait appal à la méthode .add_class() qui contient une liste des fichiers correspondants (ici trois chacun) en indiquant leur chemin absolu.

### Ajout d'un texte inconnu pour vérifier le bon fonctionnement = pour la classification du fichier

Afin de tester la bonne classification des fichiers, on insère un fichier 'inconnu' (dans ce test = "mystère.txt") et on applique la méthode .classify() qui va permettre, en sortie, de savoir à quel groupe appartient le fichier associé au k plus proches voisins selon le groupe d'appartenance.

### Sauvegarde du fichier en JSON

Pour obtenir le fichier JSON en sortie, on utilise la fonction .save_as_json(). Avec cette méthode, le rendu dans le fichier donnera un dictionnaire avec pour chaque token (clé) et sa fréquence (cvaleur) pour chaque text de chaque groupe.

### Enregistrement du fichier JSON

Pour enregistrer le fichier selon les k plus proches dans les fichiers classés, on utilise la méthode .load_as_json(). Dans in dictionnaire apparaissent les groupes d'appartenance du fichier (clé) et dans une liste la valeur de similarité (valeur).

## Difficultés et bogues

J'ai rencontré des difficultés pour la réalistion de la méthode associée au TF-IDF, qui n'est donc pas présente dans mon code. De même pour la méthode de filtrage, je n'ai pas réussi à l'initialiser.
Les bogues auxquels j'ai eu à faire étaient beaucoup d'ordre de la syntaxe, de l'appel de variables ou dans la reprise du code lui-même car les blocs ne fonctionnaient pas.

## Améliorations

Dans l'ensemble, le code fonctionne mais je pense qu'il y aurait des améliorations en termes de lisibilité, de fonctionnalité (peut être avec des blocs de codes plus courts et plus simples) avec aussi des méthodes plus précises et pointues. Bref, dans l'ensemble le code pourrait être mieux.


