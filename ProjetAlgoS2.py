#!/usr/bin/env python
# coding: utf-8

# In[19]:


# projet algo s2

import json
import math
import re

class TextVect:
    # API de la classe TextVect :
    """
    Propriétés :
    - text : str, le texte correspondant à ce vect

    Méthodes d’instance :
    - sim_cosinus(other:TextVect) : renvoie la similarité cosinus entre ce vecteur et un autre vecteur other.
    - doc2vec(filename:str) : calcule un vecteur de type TextVect à partir d’un fichier texte dont le nom est filename.
    - filtr(stopwords:list, hapax:list) : filtre les mots du texte en utilisant une liste de stopwords et une liste de mots à exclure (hapax).
    - tf_idf(docs:list) : calcule le vecteur TF-IDF pour ce texte à partir d’une liste de TextVect représentant les documents de la collection.
    """
    
    def __init__(self, filename):
    # construceur de la classe TextVect
        # ici on pose des conditions qui vont permettre de savoir à quel type de fichier on a à faire et permettre en sortie de ne pas avoir d'erreur
        if type(filename) is str: # si en entrée on a un texte 
            
            input_file = open(filename, mode="r", encoding="utf-8")
            tokens = tokenize(input_file.read(),tok_grm)  
            input_file.close()

            vector=vectorise(tokens)
            self.name = filename
        elif type(filename) is dict: # si on a un dictionnaire
            vector=filename["vec"]
            self.name = filename["nom"]
        self.vector = vector
        
    
    def sim_cosinus(self, other:'TextVect'):
    # fonction qui permet de calculer la similarite des fichiers 
        scalaire=0
        for key in self.vector:
            if key in other.vector:
                scalaire += self.vector[key] * other.vector[key]
        # calcul le carré des valeurs et on fait la somme des carrées 
        norm_a = math.sqrt(sum([value * value for value in self.vector.values()]))
        norm_b = math.sqrt(sum([value * value for value in other.vector.values()]))
        cosinus = scalaire / (norm_a * norm_b)
        return cosinus   
        
    
                

class KNNClasses:
    "classe permettant de mettre en œuvre l'algorithme de classification supervisé KNN"
    # API de la classe KNNClasses : 
    """
    Propriétés:
    - description : une string contenant la description de la classification
    - data : une liste de classes, qui correspond à la structure JSON : list de dict, 
    chaque élément de la liste est une classe représentée par un dict avec deux clés : « label » (string, le nom de la classe) 
    et « vectors » (list de dict, chaque élément de la liste est un vecteur représenté par un dict avec des clés correspondant 
    aux noms des dimensions et des valeurs flottantes)

    Méthodes d'instanciation:
    - add_class(label:str, vectors:list) : 
    ajoute une nouvelle classe à la liste data 
    label est le nom de la nouvelle classe, 
    vectors est une liste de dictionnaires représentant les vecteurs de la classe 
    Chaque dictionnaire doit avoir les mêmes clés, correspondant aux noms des dimensions, et les mêmes types de valeurs, flottantes
    - add_vector(label:str, vector:dict) : 
    ajoute un vecteur représenté par un dictionnaire au dict de la classe correspondant à label
    Le dictionnaire doit avoir les mêmes clés que les autres vecteurs de la classe et des valeurs flottantes
    - del_class(label:str) : pour supprimer la classe correspondant à label
    - save_as_json(filename:str) : 
    enregistre la liste data au format JSON dans un fichier dont le nom est filename
    - load_as_json(filename:str) : charge la liste data à partir d’un fichier JSON dont le nom est filename
    - classify(vector:dict, k:int, sim_func:callable = None) : 
    renvoie la liste des classes candidates pour le vecteur, comme liste triée de paires [label:str, sim:float]
    La liste est triée par similarité décroissante, la similarité étant la moyenne des similarités obtenues sur les vecteurs retenus 
    pour la classe correspondante dans les k plus proches voisins. Le vecteur est représenté par un dictionnaire avec des clés correspondant 
    aux noms des dimensions et des valeurs flottantes. k est le nombre de voisins les plus proches à considérer. 
    sim_func est une fonction callable qui prend en entrée deux vecteurs représentés par des dictionnaires et renvoie leur similarité. 
    Si sim_func est None, la similarité par défaut est la similarité cosinus.
    """
    
    def __init__(self, description=""):
    # constructeur de la classe KNNClasses
        self.description = description
        self.data = []

    def add_class(self, label, vectors):
        self.data.append({"label": label, "vectors": vectors})

    def add_vector(self, label, vector):
        for cls in self.data:
            if cls["label"] == label:
                cls["vectors"].append(vector)

        self.add_class(label, [vector])

    def del_class(self, label):
        self.data = [cls for cls in self.data if cls["label"] != label]

    def save_as_json(self, filename):
        f= open(filename, mode="w", encoding="utf-8")
        
        sortie={}
        for cls in self.data:
            sortie[cls['label']]=[]
            for vec in cls["vectors"]:
                # TextVect n'est pas serialisable donc on met son contenu
                sortie[cls['label']].append({"nom":vec.name, "vec":vec.vector})
        
        json.dump({"description": self.description, "data": sortie},f, ensure_ascii=False, indent='\t')
        f.close()
        
    @classmethod # fonction liee à une classe et non à l'objet
    def load_as_json(cls, filename):
        with open(filename, mode="r", encoding="utf-8") as f:
            data = json.load(f)
            obj = cls(data["description"])
            for cls in data["data"]: 
                tous_vec=[]
                for v in data["data"][cls]:
                    # on recree le TextVect
                    tous_vec.append(TextVect(v))
                obj.add_class(cls, tous_vec)
            
        return obj
            

    def classify(self, vector, k, sim_func:callable = TextVect.sim_cosinus):
        candidates = {}
        sims = []
        
        # on compare tous les vecteurs qui existent avec les classes pour voir avec lequel il est le plus proche
        for cls in self.data:
            
            for vec in cls["vectors"]:
                # ("nom_classe", sim)
                sims.append(
                    (
                        cls['label'], sim_func(vector, vec)
                    )
                )
                
        # puis on tri les vecteurs par ordre croissant (fonction .sort())
        sims.sort(reverse = True, key=lambda x : x[1])
        # on retient les k meilleurs 
        k_plus_proche=sims[:k]
        
        # on ajoute les cles comme nom de la clsse et comme valeur = indice de sim pour chaque vote
        for vote in k_plus_proche:
            if vote[0] not in candidates:
                candidates[vote[0]]=[vote[1]]
            else:
                candidates[vote[0]].append(vote[1])
                
        return candidates
    
# In[20]:


# import de focntion nécessaire au fonctionnement du code

def vectorise(tokens):
    """
    But : cette fonction récupère une liste de tokens, et renvoie un dictionnaire
    contenant le vocabulaire avec les fréquences associées
    Input :
    arg1 : tokens - list(str)
    Output :
    valeur de retour : un dict contenant les vocables (clés) et les fréq associées (valeur)
    """
    token_freq={}  # initialisation du hachage
    for token in tokens:  # parcours des tokens
        if token not in token_freq.keys():   # si on a un token
            token_freq[token]=0 
        token_freq[token]+=1 #on associe la fréquence à la clé (token)
    return token_freq

tok_grm=re.compile(r"""
    (?:etc.|p.ex.|cf.|M.)|
    \w+(?=(?:-(?:je|tu|ils?|elles?|nous|vous|leur|lui|les?|ce|t-|même|ci|là)))|
    [\w\-]+'?| 
    """,re.X)

def tokenize(text,tok_grm):
  """
  Input : 
    arg1 : un texte à tokeniser
    arg2 : une regex compilée pour la tokenisation
  Output : 
    valeur de retour : la liste des tokens
  """
  return tok_grm.findall(text)

# In[21]:

# test methodes

# creation de classes pour tester le code = à partir de textes que l'on veut classer
classes = KNNClasses("classifiction des recettes")

classes.add_class("dessert", [
    TextVect("donneestest/dessert1.txt"),
    TextVect("donneestest/dessert2.txt"),
    TextVect("donneestest/dessert3.txt"),
])

classes.add_class("entree", [
    TextVect("donneestest/entrée1.txt"),
    TextVect("donneestest/entrée2.txt"),
    TextVect("donneestest/entrée3.txt"),
])

classes.add_class("plat", [
    TextVect("donneestest/plat1.txt"),
    TextVect("donneestest/plat2.txt"),
    TextVect("donneestest/plat3.txt"),
])

# on teste le code en ajoutant un texte inconnu aux classes
myst_text=TextVect("donneestest/mystere.txt")
print(classes.classify(myst_text, 3))

print(classes.data)
classes.save_as_json("test.json")

# In[22]:

# test de la méthode de classe dont on agit directement sur la classe
classe1 = KNNClasses.load_as_json("test.json")

myst_text=TextVect("donneestest/mystere.txt")
print(classe1.classify(myst_text, 3))
